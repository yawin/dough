/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef SOLID_H
#define SOLID_H

#include "sprite.h"
#include "camera.h"
#include "interfaces/alive.h"
#include <RosquilleraReforged/rf_engine.h>

#include <SDL2/SDL.h>
#include <vector>
#include <unordered_map>
#include <map>
#include <cstdlib>
using namespace std;

class Solid;

class BoxCollider
{
  public:
    BoxCollider(Solid* owner, int _id, SDL_Rect col, bool istrigger = false);
    virtual ~BoxCollider();

    void View();
    void Hide();

    SDL_Rect collider;
    bool isTrigger = false;
    Solid* solid;

  private:
    int id;
    RF_Process* box = nullptr;
};

class Solid : public Sprite, public Alive
{
  public:
    Solid(string name = "solid");
    virtual ~Solid();

    virtual void Start();
    virtual void Awake(){}
    virtual void BeforeMove(){}
    virtual void AfterMove(){}
    virtual void Draw();

    void IsColliding(int _id, BoxCollider* collider);
    void IsTriggering(int _id, BoxCollider* collider);
    bool WasColliding(int _id, BoxCollider* collider, bool isTrigger = false);
    void CheckCollisionExit(int _id, BoxCollider* collider);

    virtual void OnCollisionEnter(int _id, BoxCollider* collider){}
    virtual void OnCollision(int _id, BoxCollider* collider){}
    virtual void OnCollisionExit(int _id, BoxCollider* collider){}

    virtual void OnTriggerEnter(int _id, BoxCollider* collider){}
    virtual void OnTrigger(int _id, BoxCollider* collider){}
    virtual void OnTriggerExit(int _id, BoxCollider* collider){}

    int addCollider(int x_offset, int y_offset, int width, int height, bool istrigger = false);
    void setCollider(int _id, int x_offset, int y_offset, int width, int height, bool istrigger = false);
    void setToTrigger(int _id = 0, bool istrigger = true);

    SDL_Rect normalizeBound(int _id);

    vector<BoxCollider> boxColliderList;
    RF_Structs::Vector2<float> force = RF_Structs::Vector2<float>(0.0, 0.0);
    bool updateCollider = true;

  private:
    unordered_map<string, bool> colliding;
};

class SolidCollisionCoroutine : public RF_Process
{
  public:
    SolidCollisionCoroutine():RF_Process("SolidCollisionCoroutine"){}
    virtual ~SolidCollisionCoroutine();

    static void Init();
    static void RegisterSolid(Solid* solid);
    static void RemoveSolid(string solid);
    static bool IsRegistered(string solid);

    virtual void Update();

  private:
    int solidCount = 0;
    map<string, Solid*> solidList;
    static string instanceid;
};

#endif //SOLID_H
